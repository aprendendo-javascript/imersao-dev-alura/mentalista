const MAX_NUMBER_ROUND = 5;
const MIN_NUMBER = 1;
const MAX_NUMBER = 20;
const MAX_GUESS_COUNT = 5;
const MAX_POINTS_BY_ROUND = 10;

const roundPHtml = document.getElementById("round-p");
const remainingGuessPHtml = document.getElementById("remaining-p");
const pointsEarnedPHtml = document.getElementById("points-earned-p");
const guessInputHtml = document.getElementById("guess-input");
let correctGuessPHtml;
let restartButton;

let currentRound;
let remainingGuesses;
let earnedPoints;
let guessResultText;
let secretNumber;

function startNewGame() {
  currentRound = 1;
  remainingGuesses = MAX_GUESS_COUNT;
  earnedPoints = 0;
  guessResultText = "";
  updateInterface();
  guessInputHtml.disabled = false;
  guessInputHtml.focus();
  secretNumber = parseInt(Math.random() * (MAX_NUMBER - MIN_NUMBER + 1)) + MIN_NUMBER;
}

function setupInterface() {
  if (correctGuessPHtml === undefined) {
    correctGuessPHtml = createAndInsertGuessResultHtml();
  }
  if (restartButton === undefined) {
    restartButton = createButton();
  }
}

function updateInterface() {
  roundPHtml.innerText = `Rodada: ${currentRound} de ${MAX_NUMBER_ROUND}`;
  remainingGuessPHtml.innerText = `Chutes Restantes: ${remainingGuesses}`;
  pointsEarnedPHtml.innerText = `Pontos conquistados: ${earnedPoints}`;
  correctGuessPHtml.innerText = guessResultText;
}

function givePoints() {
  earnedPoints += MAX_POINTS_BY_ROUND * ((remainingGuesses + 1) / MAX_GUESS_COUNT);
}

function startNewRound() {
  currentRound++;
  remainingGuesses = MAX_GUESS_COUNT;
  secretNumber = parseInt(Math.random() * (MAX_NUMBER - MIN_NUMBER + 1)) + MIN_NUMBER;
}

function insertElement(element, destinyId) {
  document.getElementById(destinyId).appendChild(element);
}

function removeElement(element) {
  element.remove();
}

function createButton() {
  const restartButton = document.createElement("input");

  restartButton.type = "button";
  restartButton.value = "Reiniciar jogo";
  restartButton.onclick = () => {
    removeElement(restartButton);
    startNewGame();
  };
  return restartButton;
}

function createAndInsertGuessResultHtml() {
  const guessResultHtml = document.createElement("p");

  guessResultHtml.className = "general-text";
  guessResultHtml.id = "correct-guess-p";
  insertElement(guessResultHtml, "container")
  return guessResultHtml;
}

//Code
setupInterface();
startNewGame();

const prepareResults = (isCorrect, msg) => {
  if (isCorrect) {
    givePoints();
  }
  guessInputHtml.value = "";
  guessResultText = msg;
};

guessInputHtml.onkeyup = (e) => {
  let key = e.which || e.keyCode;
  if (key == 13) {
    const guess = guessInputHtml.value;
    const isCorrectGuess = guess == secretNumber;

    remainingGuesses--;
    if (remainingGuesses > 0) {
      if (guess && guess % 1 === 0) {
        if (isCorrectGuess) {
          prepareResults(true, `Correto! O número alvo é ${guess}.` + (currentRound < MAX_NUMBER_ROUND ? "\nComeçando nova rodada!" : ""));
        } else if (guess && guess % 1 === 0 && (guess > MAX_NUMBER || guess < MIN_NUMBER)) {
          prepareResults(false, `Incorreto, o número ${guess} está fora dos limites do jogo`);
        } else if (guess && guess % 1 === 0 && guess > secretNumber) {
          prepareResults(false, `Incorreto, o número ${guess} é maior que o número alvo`);
        } else if (guess && guess % 1 === 0 && guess < secretNumber) {
          prepareResults(false, `Incorreto, o número ${guess} é menor que o número alvo`);
        }
      } else {
        prepareResults(false, "Incorreto, entrada inválida.");
      }
    } else if (isCorrectGuess) {
      prepareResults(true, `Correto! O número alvo é ${guess}.` + (currentRound < MAX_NUMBER_ROUND ? "\nComeçando nova rodada!" : ""));
    } else {
      prepareResults(false, `Incorreto, o número alvo era ${secretNumber}.` + (currentRound < MAX_NUMBER_ROUND ? "\nComeçando nova rodada!" : ""));
    }

    if (isCorrectGuess || remainingGuesses == 0) {
      if (currentRound < MAX_NUMBER_ROUND) {
        startNewRound();
      } else {
        const msg = guessResultText + "\n" + `\nFim de jogo! Sua pontuação final foi ${earnedPoints} pontos\n de um total possível de ${MAX_POINTS_BY_ROUND * MAX_NUMBER_ROUND} pontos`;

        prepareResults(false, msg);
        guessInputHtml.disabled = true;
        insertElement(restartButton, "container");
      }
    }
    updateInterface();
  }
};